<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);


Route::get('/test', function()
{
    $client = JonnyW\PhantomJs\Client::getInstance();
    $client->setPhantomJs('/var/www/vendor/bin/phantomjs');
    $client->setPhantomLoader('/var/www/vendor/bin/phantomloader');

    //var_dump(is_writable('/var/www/vendor/bin'));
    //$client->debug(true); // Set debug flag
    /**
     * @see JonnyW\PhantomJs\Message\Request
     **/
    $request = $client->getMessageFactory()->createRequest('http://www.techigniter.in/tutorials/disable-csrf-check-on-specific-routes-in-laravel-5/', 'GET');
    //$request = $client->getMessageFactory()->createRequest('http://google.ru', 'GET');

    /**
     * @see JonnyW\PhantomJs\Message\Response
     **/
    $response = $client->getMessageFactory()->createResponse();

    $timeout = 200000; // 20 seconds

    $request->setTimeout($timeout);

    // Send the request
    $client->send($request, $response);

    //if($response->getStatus() === 200) {

        // Dump the requested page content
        echo $response->getContent();
    echo $client->getLog(); // Output log
var_dump($response->getContentType());
   // echo file_get_contents('https://docs.angularjs.org/tutorial/step_07.html');


    //}
});