<?php
namespace App\Modules\Sitemap\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Downloads
 * @package App\Modules\Sitemap\Models
 * @author Fruty <ed.fruty@gmail.com>
 */
class Downloads extends Model
{
    /**
     * Fillable attributes by mass assignments
     *
     * @var array
     */
    protected $fillable = ['key'];
} 