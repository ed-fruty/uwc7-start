<?php namespace App\Modules\Sitemap\Models;

use Illuminate\Database\Eloquent\Model;
use League\Url\Url;

/**
 * Class SitesProcess
 *
 * @package App\Modules\Sitemap\Models
 * @author Fruty <ed.fruty@gmail.com>
 */
class SitesProcess extends Model
{
    /**
     * Table name
     *
     * @var string
     */
    protected $table = 'sites_process';

    /**
     * Mass assignment fields
     *
     * @var array
     */
    protected $fillable = ['url', 'email'];

    /**
     * Visible fields, when converting to json
     *
     * @var array
     */
    protected $visible = ['id', 'url', 'status'];

    /**
     * Url setter
     *
     * @param $value
     */
    public function setUrlAttribute($value)
    {
        $this->attributes['url'] = Url::createFromUrl($value)->getBaseUrl();
    }
}
