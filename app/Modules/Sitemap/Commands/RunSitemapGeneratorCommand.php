<?php namespace App\Modules\Sitemap\Commands;

use App\Commands\Command;
use App\Modules\Sitemap\Models\Downloads;
use App\Modules\Sitemap\Models\SitesProcess;
use App\Modules\Sitemap\Services\Generator\Generator;
use App\Modules\Sitemap\Services\SiteAnalyzer\SiteAnalyzer;
use App\Modules\Sitemap\Services\SiteAnalyzer\UrlCollection;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use Config;

/**
 * Class RunSitemapGeneratorCommand
 *
 * @package App\Modules\Sitemap\Commands
 * @author Fruty <ed.fruty@gmail.com>
 */
class RunSitemapGeneratorCommand extends Command implements ShouldBeQueued, SelfHandling
{
    /**
     * Trait with methods delete|release for job
     */
    use InteractsWithQueue;

    /**
     * Use trait for models serialization
     */
    use SerializesModels;

    /**
     * Site id
     *
     * @var int
     */
    private $siteId;

    /**
     * Site model
     *
     * @var SitesProcess
     */
    private $site;

    /**
     * Create a new command instance.
     *
     * @param int $siteId
     */
    public function __construct($siteId)
    {
        $this->siteId = $siteId;
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        /*
         * Analyze site
         */
        $collection = $this->analyze();

        /*
         * Generate map file
         */
        $key = $this->generateMap($collection);

        /*
         * Send mail notification
         */
        $this->sendNotification($key);

        /*
         * Delete file and row from database
         */
        $this->deleteSiteAndFile($key);
    }

    /**
     * Analyze site, collect urls
     *
     * @return UrlCollection
     */
    private function analyze()
    {
        $this->site = SitesProcess::findOrFail($this->siteId);
        $config = config('sitemap.analyzer', []);

        return SiteAnalyzer::create($this->site)
            ->setConfig($config)
            ->parse()
            ->getCollection();
    }

    /**
     * Generate sitemap file
     *
     * @param UrlCollection $collection
     * @return string
     */
    private function generateMap(UrlCollection $collection)
    {
        $generator = new Generator($collection);
        return $generator->make();
    }

    /**
     * Save key to the
     * @param $key
     * @deprecated
     */
    private function saveMapFileAsDownloadable($key)
    {
        // Delete old files
        Downloads::where('created_at', '<', date('Y-m-d H:i:s', strtotime('-1 day')))->delete();
        Downloads::create(['key' => $key]);
    }

    /**
     * Send email notification
     *
     * @param $key
     */
    private function sendNotification($key)
    {
        $self = $this;
        \Mail::send('sitemap::mails.download', ['key' => $key, 'siteModel' => $this->site], function($message) use($self, $key)
        {
            $message->to($self->site->email)->subject("You can download your sitemap file!");
            $message->from('uwc@test.com', 'Site map generator');
            $message->attach(config('sitemap.generator.path') . "/$key", ['as' => 'sitemap.xml']);
        });
    }

    /**
     * Delete site from database
     *
     * @param $key
     * @throws \Exception
     */
    private function deleteSiteAndFile($key)
    {
        $this->site->delete();

        if (is_file($filename = config('sitemap.generator.path') . "/$key")) {
            @unlink($filename);
        }
    }

}
