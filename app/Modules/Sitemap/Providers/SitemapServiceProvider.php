<?php namespace App\Modules\Sitemap\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;

/**
 * Class SitemapServiceProvider
 *
 * @package App\Modules\Sitemap\Providers
 * @author Fruty <ed.fruty@gmail.com>
 */
class SitemapServiceProvider extends ServiceProvider
{

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
        $this->loadRoutes();
        $this->loadViewsFrom(__DIR__ . '/../Resources/views', 'sitemap');
        // disable framework error handlers
        set_error_handler(null);
        set_exception_handler(null);
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{

	}

    /**
     * Load module routes
     *
     * @return void
     */
    private function loadRoutes()
    {
        if (is_file($filename = __DIR__ . '/../Http/routes.php')) {
            require_once $filename;
        }
    }

}
