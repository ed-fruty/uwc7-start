<?php
namespace App\Modules\Sitemap\Services\SiteAnalyzer\Loader;

/**
 * Class Page
 * @package App\Modules\Sitemap\Services\SiteAnalyzer\Loader
 * @author Fruty <ed.fruty@gmail.com>
 */
class Page
{
    /**
     * @var
     */
    protected $url;

    /**
     * @var
     */
    protected $content;

    /**
     * @var
     */
    protected $contentType;

    /**
     * @var
     */
    protected $status;

    /**
     * @var
     */
    protected $headers;

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     * @return $this
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContentType()
    {
        return $this->contentType;
    }

    /**
     * @param mixed $contentType
     * @return $this
     */
    public function setContentType($contentType)
    {
        $this->contentType = $contentType;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param mixed $headers
     * @return $this
     */
    public function setHeaders($headers)
    {
        $this->headers = $headers;
        return $this;
    }
} 