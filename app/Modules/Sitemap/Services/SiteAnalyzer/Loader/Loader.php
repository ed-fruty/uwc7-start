<?php
namespace App\Modules\Sitemap\Services\SiteAnalyzer\Loader;

use App\Modules\Sitemap\Services\SiteAnalyzer\Loader\Drivers\AbstractDriver;
use InvalidArgumentException;

/**
 * Class Loader
 * @package App\Modules\Sitemap\Services\SiteAnalyzer\Loader
 * @author Fruty <ed.fruty@gmail.com>
 */
class Loader
{
    /**
     * List of available drivers
     *
     * @var array
     */
    protected $availableDrivers = [
        'Phantom',
        'Curl',
    ];

    /**
     * Drivers instances
     *
     * @var array
     */
    protected $driverInstances = [];

    /**
     * Load page
     *
     * @param $url
     * @return Page
     */
    public function loadPage($url)
    {
        $page = false;

        if ($sleep = config('sitemap.loader.sleepBeforeRequest')) {
            echo "Sleep before request... {$sleep} sec.\n";
            sleep($sleep);
            echo "Wake up\n";
        }
        echo "Loading page {$url} ...\n";
        foreach ($this->getDrivers() as $driver) {


            $page = $this->getDriverInstance($driver)->loadPage($url);
            if ($page) {
                break;
            }

        }
       // if (! $page) {
       //     throw new \RuntimeException("Can't load url {$url}", 500);
       // }
        echo "Ok\n";
        return $page;
    }

    /**
     * @return array
     */
    protected function getDrivers()
    {
        return $this->availableDrivers;
    }

    /**
     * @param $driver
     * @return AbstractDriver
     */
    protected function getDriverInstance($driver)
    {
        if (! isset($this->driverInstances[$driver])) {
            $class = __NAMESPACE__ . "\\Drivers\\{$driver}Driver";

            if (! class_exists($class)) {
                throw new InvalidArgumentException("Loader driver {$driver} not found", 500);
            }
            $this->driverInstances[$driver] = new $class;
        }
        return $this->driverInstances[$driver];
    }
} 