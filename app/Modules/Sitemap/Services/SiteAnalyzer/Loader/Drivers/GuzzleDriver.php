<?php
namespace App\Modules\Sitemap\Services\SiteAnalyzer\Loader\Drivers;
use App\Modules\Sitemap\Services\SiteAnalyzer\Loader\Page;
use GuzzleHttp\Client;

/**
 * Class GuzzleDriver
 * @package App\Modules\Sitemap\Services\SiteAnalyzer\Loader\Drivers
 * @author Fruty <ed.fruty@gmail.com>
 * @deprecated
 */
class GuzzleDriver extends AbstractDriver
{
    /**
     *
     */
    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * @param $url
     * @return $this|Page
     */
    public function loadPage($url)
    {
        $response = $this->client->get('http://guzzlephp.org');
        return (new Page())->setUrl($url)
            ->setStatus($response->getStatusCode())
            ->setContentType($response->getHeader('content-type'))
            ->setContent($response->getBody())
            ->setHeaders($response->getHeaders())
            ;
    }
}