<?php
namespace App\Modules\Sitemap\Services\SiteAnalyzer\Loader\Drivers;
use App\Modules\Sitemap\Services\SiteAnalyzer\Loader\Page;

/**
 * Class BaseDriver
 * @package App\Modules\Sitemap\Services\SiteAnalyzer\Loader\Drivers
 * @author Fruty <ed.fruty@gmail.com>
 */
class CurlDriver extends AbstractDriver
{
    /**
     * @param string $url
     * @return Page
     */
    public function loadPage($url)
    {
        $uagent = "Opera/9.80 (Windows NT 6.1; WOW64) Presto/2.12.388 Version/12.14";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_setopt($ch, CURLOPT_USERAGENT, $uagent);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120);
        curl_setopt($ch, CURLOPT_TIMEOUT, 120);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        $response = curl_exec($ch);

        $info = curl_getinfo($ch);

        $headerText = substr($response, 0, $info['header_size']);
        $body = substr($response, $info['header_size']);


        return (new Page())->setUrl($info['url'])
            ->setStatus($info['http_code'])
            ->setContentType($info['content_type'])
            ->setContent($body)
            ->setHeaders($this->makeArrayHeaders($headerText))
            ;
    }

    /**
     * Make headers array from text
     *
     * @param $headerText
     * @return array
     */
    private function makeArrayHeaders($headerText)
    {
        $headers = array();

        foreach (explode("\r\n", $headerText) as $i => $line)
            if ($i === 0) {
                $headers['http_code'] = $line;
            } elseif (strpos($line, ':') !== false) {
                list ($key, $value) = explode(': ', $line);
                $headers[$key] = $value;
            }
        return $headers;
    }
} 