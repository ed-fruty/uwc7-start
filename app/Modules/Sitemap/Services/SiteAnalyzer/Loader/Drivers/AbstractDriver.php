<?php
namespace App\Modules\Sitemap\Services\SiteAnalyzer\Loader\Drivers;

use App\Modules\Sitemap\Services\SiteAnalyzer\Loader\Page;

/**
 * Class AbstractDriver
 * @package App\Modules\Sitemap\Services\SiteAnalyzer\Loader\Drivers
 * @author Fruty <ed.fruty@gmail.com>
 */
abstract class AbstractDriver
{
    /**
     * @param $url
     * @return Page
     */
    abstract public function loadPage($url);
} 