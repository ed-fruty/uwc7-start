<?php
namespace App\Modules\Sitemap\Services\SiteAnalyzer\Loader\Drivers;

use App\Modules\Sitemap\Services\SiteAnalyzer\Loader\Page;
use JonnyW\PhantomJs\Client;

/**
 * Class PhantomDriver
 * @package App\Modules\Sitemap\Services\SiteAnalyzer\Loader\Drivers
 * @author Fruty <ed.fruty@gmail.com>
 */
class PhantomDriver extends AbstractDriver
{
    /**
     * @var int
     */
    protected $redirectLimits = 10;

    /**
     * Load page
     *
     * @param string $url
     * @return Page
     */
    public function loadPage($url)
    {
        $client = Client::getInstance();
        $response = $client->getMessageFactory()->createResponse();
        $currentIteration = 0;
        $isRedirect = true;

        while ($isRedirect && ($this->redirectLimits < $currentIteration)) {
            $currentIteration++;

            $request = $client->getMessageFactory()->createRequest($url);
            $client->send($request, $response);
            if ($isRedirect = $response->isRedirect()) {
                $url = $response->getRedirectUrl();
            }
        }

        if ($response->getStatus() === 0 && $response->getContentType() === null) {
            // хз, некоторые сайты отказывается парсить
            // вернем false, пусть другой драйвер загрузит
            return false;
        }

        return (new Page())->setUrl($response->getUrl())
            ->setStatus($response->getStatus())
            ->setContentType($response->getContentType())
            ->setContent($response->getContent())
            ->setHeaders($response->getHeaders())
            ;

    }
} 