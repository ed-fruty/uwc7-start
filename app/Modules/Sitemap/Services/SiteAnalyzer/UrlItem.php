<?php
namespace App\Modules\Sitemap\Services\SiteAnalyzer;

/**
 * Class UrlItem
 * @package App\Modules\Sitemap\Services\SiteAnalyzer
 * @author Fruty <ed.fruty@gmail.com>
 */
class UrlItem
{
    /**
     * @var string
     */
    protected $location;

    /**
     * @var string
     */
    protected $lastMod;

    /**
     * @var string
     */
    protected $frequency;

    /**
     * @var string
     */
    protected $priority;

    /**
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param string $location
     * @return $this
     */
    public function setLocation($location)
    {
        $this->location = $location;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastMod()
    {
        return $this->lastMod;
    }

    /**
     * @param string $lastMod
     * @return $this
     */
    public function setLastMod($lastMod)
    {
        $this->lastMod = $lastMod;
        return $this;
    }

    /**
     * @return string
     */
    public function getFrequency()
    {
        return $this->frequency;
    }

    /**
     * @param string $frequency
     * @return $this
     */
    public function setFrequency($frequency)
    {
        $this->frequency = $frequency;
        return $this;
    }

    /**
     * @return string
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param string $priority
     * @return $this
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
        return $this;
    }
} 