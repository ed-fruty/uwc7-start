<?php
namespace App\Modules\Sitemap\Services\SiteAnalyzer;
use Illuminate\Support\Collection;

/**
 * Class UrlCollection
 * @package App\Modules\Sitemap\Services\SiteAnalyzer
 * @author Fruty <ed.fruty@gmail.com>
 */
class UrlCollection extends Collection
{

} 