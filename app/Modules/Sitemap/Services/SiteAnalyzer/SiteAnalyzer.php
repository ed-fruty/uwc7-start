<?php
namespace App\Modules\Sitemap\Services\SiteAnalyzer;
use App\Modules\Sitemap\Services\SiteAnalyzer\Loader\Loader;
use App\Modules\Sitemap\Services\SiteAnalyzer\Loader\Page;
use Illuminate\Database\Eloquent\Model;
use League\Url\Url;
use phpQuery;
use DateTime;

/**
 * Class SiteAnalyzer
 * @package App\Modules\Sitemap\Services\SiteAnalyzer
 * @author Fruty <ed.fruty@gmail.com>
 */
class SiteAnalyzer
{
    const FREQUENCY_NEVER = 'never';

    const FREQUENCY_YEARLY = 'early';

    const FREQUENCY_MONTHLY = 'monthly';

    const FREQUENCY_WEEKLY = 'weekly';

    const FREQUENCY_DAILY = 'daily';

    const FREQUENCY_HOURLY = 'hourly';

    /**
     * @var UrlCollection
     */
    protected $urlCollection;

    /**
     * @var int
     */
    protected $maxNestedLevel = 3;

    /**
     * @var Model
     */
    protected $siteModel;

    /**
     * List of parsed links
     *
     * @var array
     */
    protected $parsed = [];

    /**
     * List of links to parse
     *
     * @var array
     */
    protected $toParse = [];

    /**
     * Available content type
     *
     * @var string
     */
    protected $availableContentType = 'text/html';

    /**
     * @var Loader
     */
    protected $loader;

    /**
     * Current nested level
     *
     * @var mixed
     */
    protected $currentLevel;

    /**
     * @param Model $siteModel
     * @return static
     */
    public static function create(Model $siteModel)
    {
        return new static($siteModel);
    }

    /**
     * @param Model $siteModel
     */
    public function __construct(Model $siteModel)
    {
        $this->siteModel = $siteModel;
        $this->initialize();
    }


    /**
     * Initialize
     */
    protected function initialize()
    {
        $this->urlCollection = new UrlCollection();
        $this->loader = new Loader();

        $this->toParse[$this->siteModel->url] = [
            'url' => $this->siteModel->url,
            'level' => 1,
        ];
    }

    /**
     * Set configuration
     *
     * @param array $config
     * @return $this
     */
    public function setConfig(array $config = [])
    {
        $this->maxNestedLevel = $config['maxNestedLevel'] ?: $this->maxNestedLevel;
        return $this;
    }

    /**
     * Set current nested level
     *
     * @param $level
     */
    protected function setCurrentLevel($level)
    {
        $this->currentLevel = $level;
    }

    /**
     * Get current nested level
     *
     * @return mixed
     */
    protected function getCurrentLevel()
    {
        return $this->currentLevel;
    }

    /**
     * Parse
     *
     * @return $this
     */
    public function parse()
    {
        do {
            /*
             * Get current item from [this.toParse]
             */
            $current = array_shift($this->toParse);

            /*
             * Mark it parsed
             */
            $this->parsed[$current['url']] = 1;

            /*
             * Set current nested level
             */
            $this->setCurrentLevel($current['level']);

            /*
             * Load page
             */
            $page = $this->getLoader()->loadPage($current['url']);


            // parse Pages only with content type text/html and status code 200
            if ($page && $this->isPageContentTypeAccept($page) && $page->getStatus() === 200) {
                $this->resolvePage($page);
                $this->loadUrlsFromPage($page, $current['level']);
            }

        } while($this->toParse && $this->getCurrentLevel() <= $this->maxNestedLevel);
        return $this;
    }

    /**
     * @return UrlCollection
     */
    public function getCollection()
    {
        return $this->urlCollection;
    }

    /**
     * @return Loader
     */
    protected function getLoader()
    {
        return $this->loader;
    }

    /**
     * @param Page $page
     */
    protected function resolvePage(Page $page)
    {
        $priority = $this->getPagePriority();
        $lastModify = $this->getPageLastModify($page);
        $changeFreq = $this->getPageFreq($lastModify);
        $location = $page->getUrl();

        $urlItem = (new UrlItem())
            ->setPriority($priority)
            ->setFrequency($changeFreq)
            ->setLastMod($lastModify)
            ->setLocation($location);

        $this->urlCollection->push($urlItem);
    }

    /**
     * @return string
     */
    protected function getPagePriority()
    {
        switch ((int) $this->getCurrentLevel()) {
            case 1:
                return '1';
            case 2:
                return '0.8';
            default:
                return '0.5';
        }
    }

    /**
     * @param $page
     * @return bool|string
     */
    protected function getPageLastModify($page)
    {
        return isset($page->getHeaders()['Last-Modified'])
            ? date('Y-m-d', strtotime($page->getHeaders()['Last-Modified']))
            : date('Y-m-d');
    }

    /**
     * @param $lastModify
     * @return string
     */
    protected function getPageFreq($lastModify)
    {
        $current = new DateTime();
        $modify = new DateTime();
        $modify->setTimestamp(strtotime($lastModify));

        $difference = $current->diff($modify);

        if ($difference->y >= 2) {
            return self::FREQUENCY_NEVER;
        } elseif ($difference->y == 1) {
            return self::FREQUENCY_YEARLY;
        } elseif ($difference->m >= 1) {
            return self::FREQUENCY_MONTHLY;
        } elseif ($difference->d > 7) {
            return self::FREQUENCY_WEEKLY;
        } elseif ($difference->h > 1) {
            return self::FREQUENCY_DAILY;
        }
        return self::FREQUENCY_HOURLY;

    }

    /**
     * @param Page $page
     */
    protected function loadUrlsFromPage(Page $page)
    {
        phpQuery::newDocument($page->getContent(), $page->getContentType());

        foreach (pq('a') as $anchor) {
            $href = pq($anchor)->attr('href');
            $href = $this->makeFromHref($href);

            if ($href && $this->validateUrl($href) && $this->isThisHost($href) && ! $this->isParsed($href)) {
                if (! isset($this->toParse[$href])) {
                    $this->toParse[$href] = [
                        'url' => $href,
                        'level' => $this->getCurrentLevel() + 1
                    ];
                }
            }
        }
    }

    /**
     * @param $href
     * @return bool|string
     */
    protected function makeFromHref($href)
    {
        if (! $href || $href === '#' || strpos($href, 'javascript:') !== false) {
            return false;
        }

        if (strpos($href, '//') !== false) {
            ltrim($href, '//');
        }

        if (! filter_var($href, FILTER_VALIDATE_URL)) {
            $parts = parse_url($this->siteModel->url);
            if (strpos($href, $parts['host']) === false) {
                // host not found
                $href = $parts['host'] . '/' . ltrim($href, '/');
            }
            if (strpos($href, $parts['scheme']) === false) {
                // schema not found
                $href = "{$parts['scheme']}://{$href}";
            }
        }

        if ($position = strpos($href, '#') !== false) {
            $href = substr($href, 0, $position);
        }

        return filter_var($href, FILTER_SANITIZE_URL) ? $href : false;
    }

    /**
     * @param $url
     * @return bool
     */
    protected function validateUrl($url)
    {
        //return filter_var($url, FILTER_VALIDATE_URL) && $url !== '#';
        return strpos($url, '#') === false && strpos($url, 'javascript:') === false ;
    }

    /**
     * @param $url
     * @return bool
     */
    protected function isParsed($url)
    {
        $with = rtrim($url, '/') . '/';
        $without = rtrim($url, '/');
        foreach ([$url, $with, $without] as $key) {
            if (isset($this->parsed[$key])) {
                return true;
            }
        }
        return false;
        //return isset($this->parsed[$url]);
    }

    /**
     * @param $url
     * @return bool
     */
    protected function isThisHost($url)
    {
        static $main;

        if (! $main) {
            $main = Url::createFromUrl($this->siteModel->url);
        }

        $checkUrl = Url::createFromUrl($url);
        $result = $checkUrl->getBaseUrl() === $main->getBaseUrl();

        unset($checkUrl);
        return $result;
    }

    /**
     * @param $page
     * @return bool
     */
    protected function isPageContentTypeAccept($page)
    {
        return strpos($page->getContentType(), $this->availableContentType) !== false;
    }

} 