<?php
namespace App\Modules\Sitemap\Services\Generator;

use App\Modules\Sitemap\Services\SiteAnalyzer\UrlCollection;

/**
 * Class Generator
 * @package App\Modules\Sitemap\Services\Generator
 * @author Fruty <ed.fruty@gmail.com>
 */
class Generator
{
    /**
     * @param $collection
     */
    public function __construct(UrlCollection $collection)
    {
        $this->collection = $collection;
    }

    /**
     * Make a file from collection data
     *
     * @return string
     */
    public function make()
    {
        $key = uniqid();
        $xml = new \SimpleXMLElement("<urlset></urlset>");
        $xml->addAttribute("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");

        foreach ($this->collection->all() as $el) {
            $url = $xml->addChild('url');

            $url->addChild('loc', $el->getLocation());
            $url->addChild('lastmod', $el->getLastMod());
            $url->addChild('changefreq', $el->getFrequency());
            $url->addChild('priority', $el->getPriority());
        }

        $xml->saveXML(config('sitemap.generator.path') . "/{$key}");

        return $key;


    }
} 