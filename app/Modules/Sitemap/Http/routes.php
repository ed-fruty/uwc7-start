<?php

/**
 * Module routes
 */
Route::group(['namespace' => 'App\Modules\Sitemap\Http\Controllers'], function()
{
   Route::get('/', ['uses' => 'DefaultController@index']);
   Route::post('/generate', ['uses' => 'DefaultController@generate']);
});