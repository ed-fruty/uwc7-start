<?php namespace App\Modules\Sitemap\Http\Requests;

use App\Http\Requests\Request;

/**
 * Class MapGeneratorRequest
 * @package App\Modules\Sitemap\Http\Requests
 * @author Fruty <ed.fruty@gmail.com>
 */
class MapGeneratorRequest extends Request
{

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'url'   => ['required', 'url', 'regex:~^(http|https)~', 'unique:sites_process,url,NULL,id,status,1,status,0'],
            'email' => 'required|email'
		];
	}

    /**
     * Custom validation errors messages
     *
     * @return array
     */
    public function messages()
    {
        return [
            'url.unique'    => 'Sitemap generation in process for this site',
            'url.regex'     => 'Url must starts from http or https',
            'url.foobar'    => 'Custom message',
        ];
    }
}
