<?php
namespace App\Modules\Sitemap\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Sitemap\Commands\RunSitemapGeneratorCommand;
use App\Modules\Sitemap\Http\Requests\MapGeneratorRequest;
use App\Modules\Sitemap\Models\SitesProcess;
use Illuminate\Support\Facades\Queue;
use Exception;
use InvalidArgumentException;

/**
 * Class DefaultController
 * @package App\Modules\Sitemap\Http\Controllers
 * @author Fruty <ed.fruty@gmail.com>
 */
class DefaultController extends Controller
{
    /**
     * Index action
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('sitemap::index');
    }

    /**
     * Run sitemap generation process
     *
     * @param MapGeneratorRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function generate(MapGeneratorRequest $request)
    {
        try {
            // create a new item
            $model = SitesProcess::create($request->input());

            // push to queue
            $this->pushOnQueueGeneration($model->id);

            return view('sitemap::created', compact('model'));
        } catch (Exception $e) {
            return response()
                ->view('sitemap::error', compact('e'))
                ->setStatusCode(500);
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    private function pushOnQueueGeneration($id)
    {
        if ($id) {
            return Queue::push(new RunSitemapGeneratorCommand($id));
        }
        throw new InvalidArgumentException("Undefined site id");
    }
} 