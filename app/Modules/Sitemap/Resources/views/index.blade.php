@extends('sitemap::master')

@section('content')
    @if($errors->has())
    <div class="alert alert-danger">
        @foreach($errors->all() as $message)
            <li class="">{{$message}}</li>
        @endforeach
    </div>
    @endif
    <form action="/generate" method="post">
        <div class="form-group">
            <label for="url">Write a link to your site</label>
            <input type="text" name="url" class="form-control" id="url" placeholder="http://">
            <input type="hidden" name="_token" value="{!!csrf_token()!!}">
        </div>
        <div class="form-group">
            <label for="email">Write your email</label>
            <input type="text" name="email" class="form-control" id="email" placeholder="mail@example.com">
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-info col-md-12" value="Generate!"/>
        </div>
    </form>
@endsection