@extends('sitemap::master')

@section('title')
    Error was happened
@endsection

@section('content')
    <p>Something went wrong</p>
    <p>
        @if(isset($e))
            {{$e->getMessage()}}
        @elseif(isset($message))
            {{$message}}
        @endif
    </p>
@endsection