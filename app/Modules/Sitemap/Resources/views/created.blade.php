@extends('sitemap::master')

@section('title')
    Процесс генерации
@endsection

@section('content')
    <p>Sitemap generation in process for site {{$model->url}}</p>
    <p>The generated file will come to you as an attachment to your email address</p>
@endsection

