<?php
 return [
     'analyzer' => [

         /*
          * Max nested level
          */
         'maxNestedLevel' => 2,
     ],

     'loader' => [

         /*
          * Sleep before sending request
          */
         'sleepBeforeRequest' => 1
     ],

     'generator' => [

         /*
          * Max urls in file
          */
         'url' => 50000,

         /*
          * Max file size
          */
         'fileSize' => 10,

         /*
          * Files save path
          */
         'path' => base_path('storage/files')
     ]
 ];