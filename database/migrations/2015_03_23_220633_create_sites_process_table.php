<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSitesProcessTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sites_process', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();
            $table->string('url');
            $table->tinyInteger('status')->default(0);
            $table->string('email');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sites_process');
	}

}
